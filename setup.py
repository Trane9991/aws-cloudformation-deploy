from setuptools import setup

setup(
    name='aws-cloudformation-deploy',
    version='0.8.0',
    packages=['pipe', ],
    url='https://bitbucket.org/atlassian/aws-cloudformation-deploy',
    author='Atlassian',
    author_email='bitbucketci-team@atlassian.com',
    description='Pipe aws-cloudformation-deploy',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    install_requires=[
        'pyyaml==5.3.1',
        'Cerberus==1.3.2',
        'docker==4.2.0',
        'boto3==1.13.6',
    ]
)
